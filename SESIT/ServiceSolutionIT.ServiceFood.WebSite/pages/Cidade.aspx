﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="Cidade.aspx.cs" Inherits="ServiceSolutionIT.ServiceFood.WebSite.pages.CIDADE.Cidade" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12 col-xs-10 col-xs-offset-1 col-md-offset-0" >
            <div class="row col-md-12 ">
            <div class="row">
                    <ul class="breadcrumb">
                        <li>
                            <a href="Default.aspx">Home</a>
                        </li>
                        <li>
                            <a href="Cidade.aspx">Cidades</a>
                        </li>
                    </ul>

          </div>

                <div class="row">
                  <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-globe"></i> Cidades</h2>

                <div class="box-icon">
                    <%--<a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>--%>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                   <%-- <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>--%>
                </div>
            </div>
            <div class="box-content row ">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
                      <label class="color_label">Nome</label>
                        <asp:TextBox runat="server" CssClass="form-control" PlaceHolder="Digite o nome da Cidade" ID="txtNome"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
                      <label class="color_label">Código IBGE</label>
                        <asp:TextBox runat="server" CssClass="form-control" PlaceHolder="Digite o código do IBGE" ID="txtCodigoIbge"></asp:TextBox>
                    </div>
                </div>
                 <br />
                 <div class="row">
                    <div class="col-md-3 col-md-offset-2 col-xs-10 col-xs-offset-1">
                      <label class="color_label">UF</label>
                        <asp:DropDownList id="cmbUf" Runat="Server" CssClass="form-control">
                          <asp:ListItem Text="Selecione" Value=""/>
                          <asp:ListItem Text="São Paulo" Value="SP"/>
                          <asp:ListItem Text="Acre" Value="AC"/>
                          <asp:ListItem Text="Goiás" Value="GO"/>                         
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <div class="row">
                     <div class="col-md-2 col-md-offset-2 col-xs-10 col-xs-offset-1">
                        <asp:LinkButton ID="btnVoltar" runat="server" CssClass="btn btn-default btn_width"><i class="glyphicon glyphicon-repeat"></i>&nbsp;Voltar</asp:LinkButton>
                    </div>
                    <div class="hidden-lg hidden-md">
                        <br />
                        <br />
                        <br />
                    </div>
                    <div class="col-md-2 col-xs-10 col-xs-offset-1 col-md-offset-0">
                        <asp:LinkButton ID="btnSalvar" runat="server" CssClass="btn btn-primary btn_width"><i class="glyphicon glyphicon-ok"></i>&nbsp;Salvar</asp:LinkButton>
                    </div>
                    
                </div>


            </div>
        </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
