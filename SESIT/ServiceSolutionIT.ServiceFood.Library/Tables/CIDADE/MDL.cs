﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceSolutionIT.ServiceFood.Library.Tables.CIDADE
{
    class MDL
    {
        public Int32 cod_cidade { get; set; }
        public string dsc_cidade { get; set; }
        public string dsc_uf { get; set; }
        public string cod_ibge { get; set; }
    }
}
